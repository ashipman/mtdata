
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include <cstring>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef vector<string> StringVec;

//======================================================================

static void
normalise(timeval& tv)
{
    while (tv.tv_usec < 0)
    {
        tv.tv_usec += 1000000;
        tv.tv_sec  -= 1;
    }

    while (tv.tv_usec >= 1000000)
    {
        tv.tv_usec -= 1000000;
        tv.tv_sec  += 1;
    }
}



static timeval
operator+(timeval t1, int32_t secs)
{
    t1.tv_sec += secs;
    return t1;
}



static timeval
operator-(timeval t1, const timeval& t2)
{
    t1.tv_sec  -= t2.tv_sec;
    t1.tv_usec -= t2.tv_usec;
    normalise(t1);
    return t1;
}



static bool
isLE(const timeval& t1)
{
    /*  Test for less than or equal to zero.
        When normalised, a negative time would have usec >= 0 e.g. sec = -1, usec = 1000.
    */
    return t1.tv_sec < 0 || (t1.tv_sec == 0 && t1.tv_usec == 0);
}



static string
show(const timeval& t1)
{
    // for debugging
    char buf[100];
    sprintf(buf, "%ld.%03ld", t1.tv_sec, t1.tv_usec / 1000);
    return string(buf);
}



static timeval
timeOfDay()
{
    /*  get the time of day as a timeval
    */
    timeval  result;
    timespec spec;

    ::clock_gettime(CLOCK_REALTIME, &spec);
    
    result.tv_sec = spec.tv_sec;
    result.tv_usec = spec.tv_nsec / 1000;

    return result;
}



static timeval
startAt()
{
    /*  Round up the time of day to the next multiple of 10 seconds.
        Ignore leap seconds.
    */
    timespec spec;
    ::clock_gettime(CLOCK_REALTIME, &spec);
    
    time_t epoch = spec.tv_sec;
    tm* local = ::localtime(&epoch);

    auto next = (local->tm_sec / 10 + 1) * 10;
    // cout << "epoch " << epoch << ", secs " << local->tm_sec << ", next " << next << "\n";

    timeval  result;

    result.tv_sec  = spec.tv_sec;
    result.tv_usec = spec.tv_nsec / 1000;
    return result + (next - local->tm_sec);
}


//======================================================================

class Record
{
public:

    Record()
      : a_      (-1),
        b_      (-1),
        c_      (-1),
        d_      (-1),
        total_  (-1)
    {
        buffer_.reserve(200);
    }



    bool
    isReady() const
    {
        return total_ > 0;
    }



    void
    append(const char* data)
    {
        // The data is probably a single whole line but there might be more than one
        buffer_ += data;

        /*  See if we have a complete record.
            Grab all text up to the next line that starts with a backslash.
            The lines are terminated with just \n.
        */
        auto pos = buffer_.find("\\\n");

        if (pos != string::npos)
        {
            string chunk(buffer_, 0, pos + 2);
            buffer_.erase(0, pos + 2);
            handleChunk(chunk);
        }
    }



    void
    handleChunk(const string& chunk)
    {
        StringVec lines;

        splitLines(chunk, lines);

        int a = -1;
        int b = -1;
        int c = -1;
        int d = -1;
        int t = -1;

        for (auto iter = lines.begin(); iter != lines.end(); ++iter)
        {
            auto& line = *iter;
            //cout << "line: " << line << "\n";

            if (startsWith(line, "A    :"))
            {
                a = getField(line);
            }
            else
            if (startsWith(line, "B    :"))
            {
                b = getField(line);
            }
            else
            if (startsWith(line, "C    :"))
            {
                c = getField(line);
            }
            else
            if (startsWith(line, "D    :"))
            {
                d = getField(line);
            }
            else
            if (startsWith(line, "TOTAL:"))
            {
                t = getField(line);
            }
        }

        if (a > 0 && b > 0 && c > 0 && d > 0 && t > 0)
        {
            a_ = a;
            b_ = b;
            c_ = c;
            d_ = d;
            total_ = t;
        }
    }



    int
    getField(const string& line)
    {
        // Extract the field or return -1;
        string field(line, 6, 7);

        char* endptr;
        long v = ::strtol(field.c_str(), &endptr, 10);

        if (field.empty() || *endptr != 0)
        {
            v = -1;
        }

        return v;
    }


    void
    splitLines(const string& chunk, StringVec& lines)
    {
        lines.clear();
        size_t start = 0;

        for (;;)
        {
            auto end = chunk.find('\n', start);

            if (end != string::npos)
            {
                // skip the \n
                string line(chunk, start, end - start);
                lines.push_back(line);
                start = end + 1;
            }
            else
            {
                // an unterminated line, shouldn't happen
                string line(chunk, start);
                lines.push_back(line);
                break;
            }
        }
    }



    bool
    startsWith(const string& text, const char* prefix)
    {
        auto len = std::strlen(prefix);
        return text.size() >= len && std::strncmp(text.c_str(), prefix, len) == 0;
    }



    void
    printJSON()
    {
        bool valid = total_ == (a_ + b_ + c_ + d_);

        cout << "{\n";
        cout << "    \"A\": " << a_ << ",\n";
        cout << "    \"B\": " << b_ << ",\n";
        cout << "    \"C\": " << c_ << ",\n";
        cout << "    \"D\": " << d_ << ",\n";
        cout << "    \"E\": " << total_ << ",\n";
        cout << "    \"VALID\": " << (valid? "true" : "false") << "\n";
        cout << "}\n";
    }

private:
    string  buffer_;
    int     a_;
    int     b_;
    int     c_;
    int     d_;
    int     total_;
};



//======================================================================

class TTY
{
public:
    TTY(string name)
      : name_   (name),
        fd_     (-1),
        haveTm_ (false)
    {
    }



    ~TTY()
    {
        close();
    }

    

    void
    open(bool blocking = false)
    {
        fd_ = ::open(name_.c_str(), O_RDONLY | (blocking? 0 : O_NONBLOCK), 0);

        if (fd_ < 0)
        {
            auto msg = string("Cannot open tty ") + name_;
            ::perror(msg.c_str());
            exit(1);
        }

        cout << "opened " << name_ << "\n";

        int e = ::tcgetattr(fd_, &tm_);

        if (e == 0)
        {
            tm_.c_iflag = IGNCR;                // discard the \r characters
            ::cfsetspeed(&tm_, B2400);
            e = ::tcsetattr(fd_, TCSANOW, &tm_);

            if (e < 0)
            {
                auto msg = string("Cannot init tty ") + name_;
                ::perror(msg.c_str());
                exit(1);
            }

            haveTm_ = true;
        }
    }



    void
    close()
    {
        if (fd_ >= 0)
        {
            if (haveTm_)
            {
                // Attempt to restore the tty properties
                ::tcsetattr(fd_, TCSANOW, &tm_);
            }

            ::close(fd_);
            fd_ = -1;
        }
    }



    bool
    dataOrTimeout(Record& record, const timeval& stopAt)
    {
        /*  Return true if we reach a time past the stopAt time.
            Otherwise append characters to the buffer.
        */

        for (;;)
        {
            fd_set  reads;
            fd_set  writes;
            fd_set  excepts;

            FD_ZERO(&reads);
            FD_ZERO(&writes);
            FD_ZERO(&excepts);
            FD_SET(fd_, &reads);

            auto diff = stopAt - timeOfDay();

            if (isLE(diff))
            {
                return true;
            }

            int nfds = fd_ + 1;

            int e = ::select(nfds, &reads, &writes, &excepts, &diff);

            if (e > 0)
            {
                if (FD_ISSET(fd_, &reads))
                {
                    static const size_t SIZE = 1024;
                    char chunk[SIZE + 1];

                    int e = ::read(fd_, chunk, SIZE);

                    if (e > 0)
                    {
                        chunk[e] = 0;
                        record.append(chunk);
                    }
                }
            }
            else
            if (e < 0)
            {
                auto msg = string("Cannot read from tty ") + name_;
                ::perror(msg.c_str());
                exit(1);
            }
        }

        return false;
    }



    void
    rawCopy()
    {
        // for testing
        static const size_t BSIZE = 1024;
        char   buf[BSIZE +  1];

        for(;;)
        {
            int e = ::read(fd_, buf, BSIZE);

            if (e > 0)
            {
                buf[e] = 0;
                cout << buf;
            }
            else
            {
                break;
            }
        }
    }


private:
    string      name_;
    int         fd_;
    termios     tm_;
    bool        haveTm_;
};

//======================================================================

int
main(int argc, char** argv)
{
    string name = "/dev/ttyUSB1";

    if (argc >= 2)
    {
        name = argv[1];
    }

    TTY tty(name);

#if 0
    // A quick test for pass-through
    tty.open(true);
    tty.rawCopy();
#else
    tty.open();

    Record record;
    auto stopAt = startAt();

    for (;;)
    {
        if (tty.dataOrTimeout(record, stopAt))
        {
            if (record.isReady())
            {
                record.printJSON();
            }

            stopAt = stopAt + 10;
        }
    }
#endif
}
